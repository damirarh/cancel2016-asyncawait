﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3_Deadlock
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private class InnocentLookingClass
        {
            public InnocentLookingClass()
            {
                GetAsync().Wait();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnDeadlock(object sender, RoutedEventArgs e)
        {
            var result = GetAsync().Result;
            var instance = new InnocentLookingClass();
        }

        private static async Task<string> GetAsync()
        {
            await Task.Delay(500);
            return String.Empty;
        }
    }
}
